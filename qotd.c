/* See LICENSE file for copyright and license details */

#include <arpa/inet.h>
#include <errno.h>
#include <math.h>
#include <netdb.h>
#include <pwd.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include "config.h"

#define MAX_QUOTE_SZ 511

void fail(const char *const fmt, ...)
{
	int e = errno;
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	fprintf(stderr, ": %s\n", strerror(e));
	exit(EXIT_FAILURE);
}
void sigchld_handle(int sig)
{
	int e = errno;
	while(waitpid(-1, NULL, WNOHANG) > 0);
	errno = e;
}
/* returns 0 on success and -1 on failure */
int install_sighandler(void)
{
	struct sigaction sa;
	sa.sa_handler = sigchld_handle;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	return sigaction(SIGCHLD, &sa, NULL);
}
/* returns bound servinfo on success and NULL on failure */
struct addrinfo *get_sockfd(struct addrinfo *servinfo, int *sockfd)
{
	struct addrinfo *p;
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if((*sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("socket()");
			continue;
		}
		int yes = 1;
		if(setsockopt(*sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof yes) == -1)
			perror("Could not set socket for reuse");
		if(bind(*sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(*sockfd);
			fprintf(stderr, "bind(%d): %s\n", atoi(port), strerror(errno));
			continue;
		}
		break;
	}
	return p;
}
FILE *urandom = NULL;
off_t get_random(off_t file_size)
{
	off_t mask = 0;
	off_t msb = 1 + log2(file_size);
	for(int i = 0; i < msb; ++i)
		mask |= 1 << i;

	off_t ret;
	do{
		fread(&ret, sizeof ret, 1, urandom);
		if(ferror(urandom))
			perror("Could not get random number");
		ret &= mask;
	}while(ret > file_size);
	return ret;
}
/*
 * I chose this way of picking a quote, because it requires
 * a minimum of file parsing.
 * To even out the quote distribution:
 *  - count the quotes
 *  - rand = get_random(count)
 *  - index in rand quotes
 * On average that would have the algorithm get every byte of the
 * fortune file 1.5 times. Since this function is only supposed to
 * be run once a day or less, this would only be a problem on the
 * first qotd request of the day.
 *
 * TODO: use strfile(8) for random access
 */
struct string { char *str; int len; };
FILE *fortune_fp = NULL;
off_t fortune_sz = 0;
void get_fortune(struct string *q)
{
	FILE *f = fortune_fp;
	off_t rand_num = get_random(fortune_sz);
	fseeko(f, rand_num, SEEK_SET);
	int c = 0;
	while(c != '%' && c != EOF)
		c = fgetc(f);
	if(c == '%')
		fgetc(f);/* Skip the newline */
	else
		fseeko(f, 0, SEEK_SET);/* Hit EOF *///TODO: check this assertion
	static char buff[MAX_QUOTE_SZ];
	fread(buff, 1, MAX_QUOTE_SZ, f);
	buff[MAX_QUOTE_SZ - 1] = '\0';
	strtok(buff, "%");
	q->str = buff;
	q->len = strlen(buff);
}
FILE *log_fp = NULL;
void log_conn(const struct sockaddr_storage *addr, struct tm *t)
{
	const void *v = addr;
	char ip_addr[INET6_ADDRSTRLEN];
	if(addr->ss_family == AF_INET) {
		const struct sockaddr_in *ipv4 = v;
		inet_ntop(addr->ss_family, &ipv4->sin_addr, ip_addr, sizeof ip_addr);
	} else {
		const struct sockaddr_in6 *ipv6 = v;
		inet_ntop(addr->ss_family, &ipv6->sin6_addr, ip_addr, sizeof ip_addr);
	}
	char time[30];
	strftime(time, 30, "%F %T", t);
	fprintf(log_fp, "%s from %s\n", time, ip_addr);
	fseek(log_fp, 0, SEEK_END);
}
int main(int argc, char *argv[])
{
	/* Prepare globals */
	if(!(log_fp = fopen(log_path, "a")))
		fprintf(stderr, "Cannot open \"%s\": %s\n", log_path, strerror(errno));
	if(!(urandom = fopen("/dev/urandom", "r")))
		fail("Cannot open \"/dev/urandom\"");
	if(!(fortune_fp = fopen(fortune_path, "r")))
		fail("Cannot open \"%s\"", fortune_path);
	fseeko(fortune_fp, 0, SEEK_END);
	fortune_sz = ftello(fortune_fp);

	struct addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC; /* Don't prefer ipv4 or ipv6 */
	hints.ai_socktype = SOCK_STREAM; /* This excludes UDP */

	int err;
	struct addrinfo *servinfo;
	if((err = getaddrinfo(hostname, port, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo(): %s\n", gai_strerror(err));
		return EXIT_FAILURE;
	}
	int sockfd;
	if(!get_sockfd(servinfo, &sockfd)) {
		fprintf(stderr, "No sutable internet connection");
		return EXIT_FAILURE;
	}
	freeaddrinfo(servinfo);
	if(!getuid()) {
		struct passwd *p = getpwnam("nobody");
		if(setuid(p->pw_uid))
			perror("setuid()");
	}
	if(listen(sockfd, connection_backlog) == -1)
		fail("listen()");
	if(install_sighandler())
		fail("sigaction()");
	puts("Waiting for connections");
	int day = 500; /* Out of tm_yday range */
	while(1) {
		struct string quote;
		struct sockaddr_storage their_addr;
		socklen_t sin_size = sizeof their_addr;
		int conn = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
		time_t t = time(NULL);
		struct tm *lt = localtime(&t);
		int day_now = lt->tm_yday;
		if((day_now != day)) {
			get_fortune(&quote);
			day = day_now;
		}
		if(conn == -1) {
			perror("accept()");
			continue;
		}
		log_conn(&their_addr, lt);
		if(!fork()) {
			if(send(conn, quote.str, quote.len, 0) == -1)
				perror("send()");
			return EXIT_SUCCESS;
		}
		close(conn);
	}
	return 0;
}
