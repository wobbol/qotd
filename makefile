# See LICENSE file for copyright and license details

qotd: config.h qotd.c
	c99 -D_DEFAULT_SOURCE -Wall -pedantic -g qotd.c -lm -o qotd

config.h:
	cp config.def.h config.h

clean:
	rm qotd
