/* See LICENSE file for copyright and license details */

const char *const fortune_path = "./fortunes";
const char *const log_path = "./log";
const char *const port = "17";
const char *const hostname = "127.0.0.1";
const int connection_backlog = 10;
