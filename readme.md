## Quote Of The Day server

Pick a quote every day and send the text over a TCP to the connecting host.
Conforms to [rfc865](https://tools.ietf.org/html/rfc865) in all ways, except UDP is not supported.

A fortunes file is needed to run this software.
Get one from
[https://github.com/bmc/fortunes](https://github.com/bmc/fortunes)
or
[http://www.bsdfortune.com/fortunes](http://www.bsdfortune.com/fortunes/).

To use the default settings, compile and run:
``` sh
% make
% ./qotd
```
To configure:
``` sh
% cp config{.def,}.h
% $(EDITOR) config.h
```
